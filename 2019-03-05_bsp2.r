#R version 3.3.2

rvar <- function(x) {
    var(x)*((length(x)-1)/length(x))
}
rcov <- function(x,y) {
    cov(x,y)*((length(x)-1)/length(x))
}
print("Hello, world!")
x <- c(1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3)
y <- c(3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1)

u1 <- mean(x)
print(u1)
sig1 <- rvar(x)
print(sig1)

sig2 <- rvar(y)
print(sig2)

cvar <- rcov(x,y)
print(cvar)
