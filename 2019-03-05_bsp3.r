
library(entropy)


x <- c(1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,3,3)
y <- c(3,4,5,6,7,8,9,0,1,2,3,4,5,6,7,8,9,0,1)

Hx <- entropy(table(x),unit="log2")
print("H(x)")
print(Hx)
print(table(x))

Hy <- entropy(table(y),unit="log2")
print("H(y)")
print(Hy)
print(table(y))

Hxy <- entropy(table(x,y),unit="log2")
print("H(x,y)")
print(Hxy)
print(table(x,y))

Hxoy <- Hxy-Hy
print("H(x|y)")
print(Hxoy)

Hyox <- Hxy-Hx
print("H(y|x)")
print(Hyox)

Ixy <- Hx+Hy-Hxy
print("I(x,y)")
print(Ixy)
